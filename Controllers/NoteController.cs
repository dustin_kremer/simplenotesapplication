using System.Collections.Generic;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

using SimpleNotesApplication.Repositories;
using SimpleNotesApplication.Models;

namespace SimpleNotesApplication.Controllers
{
    [Route("API/[controller]")]
    public class NoteController : Controller
    {
        private INoteRepository noteRepository;

        public NoteController(INoteRepository noteRepository)
        {
            this.noteRepository = noteRepository;
        }

        [HttpGet("Login/{username}:{password}")]
        public IActionResult Login(string username, string password)
        {
            if (password != "12345")
            {
                return NotFound();
            }

            var identity = new ClaimsIdentity("password");
            identity.AddClaim(new Claim(ClaimTypes.Name, "DefaultUser"));
            identity.AddClaim(new Claim(ClaimTypes.Role, username));
            HttpContext.Authentication.SignInAsync("CustomAuth", new ClaimsPrincipal(identity)).Wait();

            return new ObjectResult("success");
        }

        [HttpGet]
        [Authorize(Roles = "User")]
        public IEnumerable<Note> GetAll()
        {
            return noteRepository.GetAll();
        }

        [HttpGet("{id}", Name = "GetNote")]
        [Authorize(Roles = "User")]
        public IActionResult GetById(string id)
        {
            var item = noteRepository.Find(id);
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public IActionResult Create([FromBody] Note item)
        {
            if (item == null)
            {
                return BadRequest();
            }
            noteRepository.Add(item);
            return CreatedAtRoute("GetNote", new { controller = "Note", id = item.Key }, item);
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "Admin")]
        public void Delete(string id)
        {
            noteRepository.Remove(id);
        }
    }
}