using System.Collections.Generic;
using SimpleNotesApplication.Models;

namespace SimpleNotesApplication.Repositories
{
    public class NoteRepository : INoteRepository
    {
        private List<Note> notes;

        public NoteRepository()
        {
            notes = new List<Note>();
        }

        public void Add(Note item)
        {
            item.Key=(notes.Count+1).ToString();
            notes.Add(item);
        }

        public Note Find(string id)
        {
            return notes.Find(n=>n.Key==id);
        }

        public IEnumerable<Note> GetAll()
        {
            return notes.AsReadOnly();
        }

        public void Remove(string id)
        {
            notes.RemoveAll(n=>n.Key==id);
        }
    }
}