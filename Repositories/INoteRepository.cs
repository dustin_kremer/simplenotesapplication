using System.Collections.Generic;
using SimpleNotesApplication.Models;

namespace SimpleNotesApplication.Repositories
{
    public interface INoteRepository
    {
        IEnumerable<Note> GetAll();
        Note Find(string id);
        void Add(Note item);
        void Remove(string id);
    }
}